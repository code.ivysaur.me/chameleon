# chameleon

![](https://img.shields.io/badge/written%20in-golang-blue)

A codec for the Chameleon compression format.

## Installing

Either extract the provided archive into your `$GOPATH`, or run `go get -u code.ivysaur.me/chameleon`.

## Usage

See `chameleon_test.go` for example usage.

## See Also

- http://cbloomrants.blogspot.co.nz/2015/03/03-25-15-my-chameleon.html

## Changelog

2016-07-17 v160717A
- Initial release


## Download

- [⬇️ chameleon-v160717A.7z](dist-archive/chameleon-v160717A.7z) *(2.59 KiB)*
- [⬇️ bundle.git](dist-archive/bundle.git) *(5.11 KiB)*
